/**
 * 
 */
package org.stevejen.java8experiments;

import java.util.List;
import java.util.function.Consumer;

/**
 * Class for testing basic lambda functionality
 * @author Stephen D'Souza
 *
 */
public class TestBasicLambda {
//	Consumer<Object> consumer = x -> System.out.println(x);
	Consumer<Object> consumer = System.out::println;
	
	void printCollectionContents(final List<Object> collection) {
		collection.forEach(consumer);
	}
	
	public static void main(String[] args) {
		
		
	}

}
